-- 118.70.17.132

CREATE DATABASE vertx_playground;

DROP TABLE post;
CREATE TABLE post
(
  id      uuid DEFAULT gen_random_uuid() PRIMARY KEY,
  title   VARCHAR(255) UNIQUE NOT NULL,
  content TEXT                NOT NULL
  -- created_at TIMESTAMP           NOT NULL
);
