package edu.vertxplayground.starter;

import edu.vertxplayground.starter.config.DataInitializer;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.pgclient.PgConnectOptions;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.PoolOptions;

import java.util.logging.Level;
import java.util.logging.Logger;

public class MainVerticle extends AbstractVerticle {
  private static final Logger LOGGER = Logger.getLogger(MainVerticle.class.getName());

  @Override
  public void start(Promise<Void> startPromise) {
    LOGGER.log(Level.INFO, "Starting HTTP server ...");
    // setupLogging();

    // Create a PgPool instance
    var pgPool = pgPool();

    // Create PostRepository
    var postRepository = PostRepository.create(pgPool);

    // Create PostHandler
    var postHandlers = PostsHandler.create(postRepository);

    // Initializing the sample data
    var initializer = DataInitializer.create(pgPool);
    initializer.run();

    // Configure routes
    var router = routes(postHandlers);

    // Create HTTP server
    vertx.createHttpServer()
      // Handle every request using the router
      .requestHandler(router)
      .listen(8888)
      // On Success
      .onSuccess(server -> {
        startPromise.complete();
        System.out.println("HTTP server started on port 8888");
      })
      // On Failure
      .onFailure(event -> {
        startPromise.fail(event);
        System.out.println("Failed to start HTTP server:" + event.getMessage());
      });
  }

  // Create Routes
  private Router routes(PostsHandler handlers) {
    // Create a Router
    Router router = Router.router(vertx);

    // Register BodyHandler globally
    router.get("/posts")
      .produces("application/json")
      .handler(handlers::all);
    router.post("/posts")
      .consumes("application/json")
      .handler(BodyHandler.create())
      .handler(handlers::save);
    router.get("/post/:id")
      .produces("application/json")
      .handler(handlers::get)
      .failureHandler(frc -> frc.response().setStatusCode(404).end());
    router.put("/posts/:id")
      .consumes("application/json")
      .handler(BodyHandler.create())
      .handler(handlers::update);
    router.delete("/posts/:id")
      .handler(handlers::delete);

    return router;
  }

  private PgPool pgPool() {
    PgConnectOptions connectOptions = new PgConnectOptions()
      .setPort(5432)
      .setHost("118.70.17.132")
      .setDatabase("vertx_playground")
      .setUser("postgres")
      .setPassword("123456aA@");

    // PoolOptions
    PoolOptions poolOptions = new PoolOptions().setMaxSize(5);

    // Create & return the pool from the data object
    return PgPool.pool(vertx, connectOptions, poolOptions);
  }
}
