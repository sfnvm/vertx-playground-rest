package edu.vertxplayground.starter;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;
import java.util.UUID;

public class Post {
  private UUID id;
  private String title;
  private String content;

  @JsonProperty("created_at")
  private LocalDateTime createdAt;

  public Post(UUID id, String title, String content, LocalDateTime createdAt) {
    this.id = id;
    this.title = title;
    this.content = content;
    this.createdAt = createdAt;
  }

  public Post(String title, String content) {
    this.title = title;
    this.content = content;
  }

  public UUID getId() {
    return this.id;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getTitle() {
    return title;
  }

  public String getContent() {
    return content;
  }

  public static Post of(String title, String content) {
    return new Post(title, content);
  }

  public static Post of(UUID id, String title, String content, LocalDateTime createdAt) {
    return new Post(id, title, content, createdAt);
  }
}
