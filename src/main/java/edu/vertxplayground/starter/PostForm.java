package edu.vertxplayground.starter;

public class PostForm {
  private String title;
  private String content;

  public void setTitle(String title) {
    this.title = title;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getTitle() {
    return title;
  }

  public String getContent() {
    return content;
  }

  public static Post of(String title, String content) {
    return new Post(title, content);
  }
}
