package edu.vertxplayground.starter.config;

import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.Tuple;

import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.StreamSupport;

public class DataInitializer {

  private final static Logger LOGGER = Logger.getLogger(DataInitializer.class.getName());

  private final PgPool client;

  public DataInitializer(PgPool client) {
    this.client = client;
  }

  public static DataInitializer create(PgPool client) {
    return new DataInitializer(client);
  }

  public void run() {
    LOGGER.info("Data initialization is starting ...");

    Tuple first = Tuple.of(UUID.randomUUID(), "Hello Quarkus", "My first post of Quarkus");
    Tuple second = Tuple.of(UUID.randomUUID(), "Hello Again, Quarkus", "My second post of Quarkus");

    client
      .withTransaction(
        conn -> conn.query("DELETE FROM post")
          .execute()
          .flatMap(r -> conn.preparedQuery("INSERT INTO post (id, title, content) VALUES ($1, $2, $3)")
            .executeBatch(List.of(first, second)))
          .flatMap(r -> conn.query("SELECT * FROM post").execute())
      )
      .onSuccess(data -> StreamSupport
        .stream(data.spliterator(), true)
        .forEach(row -> LOGGER.log(Level.INFO, "saved data: {0}", new Object[]{row.toJson()})))
      .onComplete(
        r -> {
          // client.close(); // Will block the application
          LOGGER.info("Data initialization is completed!");
        }
      )
      .onFailure(
        throwable -> {
          LOGGER.warning("Data initialization is failed: " + throwable.getMessage());
          LOGGER.info(throwable.toString());
        }
      );
  }

}
